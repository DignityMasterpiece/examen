import "./configuration/environment";
import express from "express";
import morgan from "morgan";
import errorHandler from "./middlewares/errorHandler";
import helmet from "helmet";
import cors from "cors";
import compression from "compression";
import passport from "passport";
import getRoutes from "./router";
import MongoConnection from "./configuration/mongo";
const mongoConnection = new MongoConnection();
mongoConnection.connect();
const router = express.Router();
const app = express();
app.use(compression());
//Middlewares
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use(passport.initialize());
app.use(
  cors({
    credentials: true,
    origin: "*",
  })
);

const routes = getRoutes("");
routes.forEach((route) => {
  let prefix = route.prefix.includes("index")
    ? `/api${route.prefix.substring(0, route.prefix.length - 5)}`
    : `/api${route.prefix}`;
  app.use(prefix, [], require(`./routes${route.file}`));
});

app.use(
  router.all("*", (req, res, next) => {
    res.status(404).send({
      message: "not found",
      error: true,
    });
  })
);

if (app.get("env") === "development") {
  app.use(errorHandler.errorDev);
} else {
  app.use(errorHandler.error);
}
export default app;
