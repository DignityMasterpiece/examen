import { config } from "dotenv";
import { existsSync } from "fs";
const envFile = ".env";
const envFileExsist = existsSync(envFile);
if (!envFileExsist) throw new Error("Need to have a env file");
config({ path: envFile });
