import mongoose from "mongoose";
export default class MongoConnection {
  constructor() {}
  connect = async () => {
    try {
      let connectionString: string = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;
      let connectionOptions: any = {};
      if (process.env.MONGO_USER && process.env.MONGO_PASS) {
        connectionOptions = {
          ...connectionOptions,
          authSource: process.env.MONGO_AUTH_SOURCE || "admin",
          user: process.env.MONGO_USER,
          pass: process.env.MONGO_PASS
        };
      }

      // console.log(connectionString, connectionOptions);
      return await mongoose.connect(connectionString, connectionOptions);
    } catch (error) {
      throw error;
    }
  };

  disconnect = () => {
    mongoose.connection.close();
  };
}
