import mongoose from "mongoose";
export default {
  getCarsByPropertiesAggregate: (match: object) => {
    let aggregate = [
      {
        $match: match,
      }, {
        $project: {
          model: 1,
          status: 1,
          color: 1,
        }
      }
    ]

    return aggregate;
  },
};
