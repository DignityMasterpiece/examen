class CustomError extends Error {
  public status: number;
  constructor(status: number, ...params) {
    // Pass remaining arguments (including vendor specific ones) to parent constructor
    super(...params);
    this.status = status;
  }
}

export default CustomError;
