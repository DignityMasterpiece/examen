
/**
* @desc    Send any success response
*
* @param   {string} message
* @param   {object | array} result
* @param   {number} statusCode
*/
export const successFormatter = (message: string, result: any, statusCode: number) => {
  return {
    message,
    error: false,
    code: statusCode,
    result,
  };
};



