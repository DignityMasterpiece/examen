
import { Types } from "mongoose";
/**
 * @description In this method we sanitize the id to avoid any
 * association to mongo by the service consumer, it can be used
 * to add more of this.
 */
const getSanitizedMatch = (match: object) => {
  if (match.hasOwnProperty('id')) {
    match['_id'] = new Types.ObjectId(match['id'])
    delete match['id']
  }
  return match
}
export default {
  getSanitizedMatch,
};
