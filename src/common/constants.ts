export default abstract class Constants {
  static readonly DEFAULT_TIME_ZONE: string = "America/Mexico_City";
  static readonly DEFAULT_TIME_FORMAT: string = "YYYY-MM-DD HH:mm:ss";
}
