import { NextFunction, Request, Response } from "express";
import * as yup from "yup";
import { _status_car } from "../../../modules/cars/cars.entity";
import getValidator from "../getValidator";
import { getCarSchema } from "./get.validator";
const validator = getValidator(getCarSchema)
export default async (req: Request, res: Response, next: NextFunction) => {
  const { body } = req;
  try {
    await validator(body)
    next();
  } catch (e) {
    res.status(422).json({ error: e.errors.join(', ') });
  }
}
