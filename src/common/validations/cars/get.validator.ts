import { NextFunction, Request, Response } from "express";
import * as yup from 'yup';
import { _status_car } from '../../../modules/cars/cars.entity';
import getValidator from '../getValidator';
const getCarSchema = yup.object({
  data: yup.object({
    model: yup.string(),
    color: yup.string(),
    status: yup.mixed()
      .oneOf(Object.values(_status_car)),
    year: yup
      .string()
      .matches(/^[0-9]{4}$/, "Not a valid year"),
  })
});
const validator = getValidator(getCarSchema)
export default async (req: Request, res: Response, next: NextFunction) => {
  const { query } = req;
  try {
    await validator(query)
    next();
  } catch (e) {
    res.status(422).json({ error: e.errors.join(', ') });
  }
}
export {
  getCarSchema,
}
