import { NextFunction, Request, Response } from "express";
import * as yup from "yup";
import { _status_car } from "../../../modules/cars/cars.entity";
import getValidator from "../getValidator"
const postCarSchema = yup.object({
  data: yup.object({
    model: yup.string().required("model is required"),
    color: yup.string().required("color is required"),
    status: yup.mixed()
      .oneOf(Object.values(_status_car))
      .required("status is required"),
    year: yup
      .string()
      .matches(/^[0-9]{4}$/, "Not a valid year")
      .required("year is required"),
  }),
});
const validator = getValidator(postCarSchema)
export default async (req: Request, res: Response, next: NextFunction) => {
  const { body } = req;
  try {
    await validator(body)
    next();
  } catch (e) {
    res.status(422).json({ error: e.errors.join(', ') });
  }
}
export {
  postCarSchema,
}
