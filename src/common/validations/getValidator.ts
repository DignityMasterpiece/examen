import { SchemaOf } from "yup";
const validate = (schema: SchemaOf<any>) => async (data: Object) => {
  try {
    await schema.validate({ data });
    return;
  } catch (err) {
    throw err;
  }
};
export default validate
