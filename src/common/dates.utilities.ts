import moment from "moment-timezone";
import Constants from "./constants";
const getFormattedDate = (date?: Date) =>
  moment(date)
    .tz(Constants.DEFAULT_TIME_ZONE)
    .format(Constants.DEFAULT_TIME_FORMAT);
export default {
  getFormattedDate,
};
