import express from "express";
import CarsController from "../../modules/cars/cars.controller";
const route = express.Router();
import postValidator from '../../common/validations/cars/post.validator';
import getValidator from '../../common/validations/cars/get.validator';
import putValidator from '../../common/validations/cars/put.validator';
route.post('/', [postValidator], CarsController.post)
route.get('/', [getValidator], CarsController.get)
route.put('/:id', [putValidator], CarsController.putById)

module.exports = route;
