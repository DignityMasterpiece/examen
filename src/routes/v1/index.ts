import express, { Request, Response, NextFunction } from "express";
const route = express.Router();
route.get("/health-check", (req: Request, res: Response, next: NextFunction) => {
  res.send("alive");
});

module.exports = route;
