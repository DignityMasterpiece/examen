import { NextFunction, Request, Response } from "express";
import { successFormatter } from "../../common/api.response";
import ICar from "../../modules/cars/cars.entity";
import CarsService from "./cars.service";

class CarsController {
  async post(req: Request, res: Response, next: NextFunction) {
    try {
      let car: ICar = req.body;
      const carSaved = await CarsService.post(car);
      res.json(successFormatter('car created successfully', carSaved, 200));
    } catch (error) {
      next(error);
    }
  }
  async get(req: Request, res: Response, next: NextFunction) {
    try {
      const toSearch: object = req.query;
      const carSaved = await CarsService.get(toSearch);
      res.json(successFormatter('search was successfully', carSaved, 200));
    } catch (error) {
      next(error);
    }
  }

  async putById(req: Request, res: Response, next: NextFunction) {
    try {
      const toUpdate = req.body;
      const id = req.params.id;
      const carSaved = await CarsService.put({ _id: id }, toUpdate);
      res.json(successFormatter('cars was updated successfully', carSaved, 200));
    } catch (error) {
      next(error);
    }
  }
}
export default new CarsController();

