import mongoose from "mongoose";
import Icar, { _status_car } from "./cars.entity";

const CarSchema = new mongoose.Schema(
  {
    model: {
      type: String,
      trim: true,
      required: true,
    },
    year: {
      type: String,
      trim: true,
      required: true,
    },
    status: {
      type: String,
      trim: true,
      required: true,
      enum: _status_car
    },
    color: {
      type: String,
      trim: true,
      required: true,
    },
    creationDate: {
      type: String,
      required: true,
    },
  },
  {
    collection: "car",
    versionKey: false
  }
)
  .index({
    status: 1,
  });
const CarModel = mongoose.model<Icar>("Message", CarSchema);
export default CarModel;
export { CarSchema };

