import ICar from "./cars.entity";
import CarModel from "./cars.model";
import DatesUtilities from "../../common/dates.utilities";
import MongoUtilities from "../../common/mongo.utilities";
import querys from "../../common/queries/cars";
const { getFormattedDate } = DatesUtilities
const { getSanitizedMatch } = MongoUtilities
class MessageService {
  constructor() {
  }
  async post(car: ICar): Promise<ICar> {
    try {
      car.creationDate = getFormattedDate();
      const newCar = new CarModel(car)
      return await newCar.save()
    } catch (error) {
      throw error;
    }
  }

  async get(toSearch: object): Promise<ICar[]> {
    try {
      const match = getSanitizedMatch(toSearch)
      return await CarModel
        .aggregate(querys.getCarsByPropertiesAggregate(match))
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param match
   * @param toUpdate
   * @returns Promise<ICar>
   * @description: this method updates the car by whatever properties are passed
   */
  async put(match, toUpdate: object): Promise<ICar> {
    try {
      return await CarModel
        .findOneAndUpdate(match, toUpdate, { new: true })
        .lean()
    } catch (error) {
      throw error;
    }
  }

}
export default new MessageService();
