import mongoose from "mongoose";
export default interface ICar {
  _id?: mongoose.Types.ObjectId;
  model: string;
  year: number;
  status: _status_car;
  color: string;
  creationDate: string;
}
export enum _status_car {
  available = "available",
  rented = "rented",
  sold = "sold",
}
