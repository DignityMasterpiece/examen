import fs from "fs";
const getRoute = (directory: string) => {
  let routes = [];
  const files = fs.readdirSync(`${__dirname}/routes/${directory}`);
  files.forEach((file) => {
    if (
      fs.lstatSync(`${__dirname}/routes/${directory}/` + file).isDirectory()
    ) {
      routes.push(getRoute(directory + "/" + file));
    } else {
      routes.push({
        prefix: directory + "/" + file.split(".")[0],
        file: directory + "/" + file,
      });
    }
  });
  return routes.reduce((acc, el) => acc.concat(el), []);
};

export default getRoute;
