export default {
  errorDev: (err, req, res, next) => {
    // Shows errors with extra information
    let status = err.status || 500;
    res.status(status).send({
      message: err.message,
      error: "true",
      errors: err.errors || undefined,
      stack: err.stack,
    });
    return;
  },

  error: (err, req, res, next) => {
    // Shows only error message
    let status = err.status || 500;
    res.status(status).send({
      message: err.message,
      error: true,
      errors: err.errors || undefined,
    });
    return;
  },
};
