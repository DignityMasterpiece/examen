import app from "./app";
import http from "http";
app.set("port", process.env.PORT || 3000);
const port = app.get("port");
const server = http.createServer(app);

server.listen(port, () => {
  console.log(`server start on port ${port} - environment ${app.get("env")}`);
});
