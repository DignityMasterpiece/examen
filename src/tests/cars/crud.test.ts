import supertest from "supertest";
import app from "../../app";
import MongoConnection from "../../configuration/mongo";
import Icar, { _status_car } from "../../modules/cars/cars.entity";
import DatesUtilities from "../../common/dates.utilities";
const { getFormattedDate } = DatesUtilities;
const request = supertest.agent(app);

afterAll(async () => {
  const mongoConnection = new MongoConnection();
  await mongoConnection.disconnect();
})

describe("Car endpoints", () => {
  const carToSave: Icar = {
    year: 1989,
    status: _status_car.available,
    color: "red",
    model: "Porche 911",
    creationDate: getFormattedDate(),
  }
  let carSaved: any = {}
  describe("Creating a car", () => {
    test("Status code 200", async () => {
      let res = await request
        .post("/api/v1/cars")
        .send(carToSave);
      // .set("Authorization", token); // <-- This is the important line when use a jwt authentication in authorization header
      let { body: { result }, statusCode } = res;
      carSaved = result;
      expect(statusCode).toEqual(200);
    })
  });

  describe("Getting cars", () => {
    test("Should be same created in test.", async () => {
      let res = await request
        .get(`/api/v1/cars?id=${carSaved._id}`)
      let { body: { result } } = res;
      expect(result[0]?._id).toEqual(carSaved._id);
    })
  })

  describe("Updating cars", () => {
    test("Should be the same created car in test, now from rented.", async () => {
      let res = await request
        .put(`/api/v1/cars/${carSaved._id}`).send({
          status: _status_car.rented
        })
      let { body: { result } } = res;
      expect(result).toEqual({
        ...carSaved,
        status: _status_car.rented
      });
    })
  })
})


