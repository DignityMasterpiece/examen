module.exports = {
  "verbose": true,
  "testRegex": ".*.test.ts",
  "testEnvironment": "node",
  "coveragePathIgnorePatterns": [
    "/node_modules/",
    "/dist/"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
  // "reporters": ["<rootDir>/src/tests/healthCheck.js"]
}
