FROM node:16.14-alpine
WORKDIR /opt/project
RUN npm install -g pm2 typescript
CMD npm install && npm run production && pm2 logs
